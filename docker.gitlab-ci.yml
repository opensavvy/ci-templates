
# Sets up Docker for the current job, and authenticates to the GitLab Container Registry.
#
# Variables:
# - docker_version: tag of the Docker version to use (e.g. 20.10)
# - docker_dind_version: tag of the Docker-in-Docker version to use (e.g. 20.10-dind)
.os.docker.dind:
  variables:
    docker_version: "25.0-cli"
    docker_dind_version: "25.0-dind"
    DOCKER_BUILDKIT: 1

  image: docker:$docker_version

  services:
    - name: docker:$docker_dind_version
      alias: docker

  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY

  tags:
    - docker

# Builds a Docker image, and pushes it to the GitLab Container Registry.
#
# Variables:
# - dockerfile: path to the Dockerfile (default: Dockerfile at the project root)
# - context: path to the directory from which Docker should take files (default: project root)
# - image: name of the created image
# - project_version: tag of the created image (default: IID of the pipeline with the prefix "build-")
#
# See also the variables from:
# - .os.docker.dind
.os.docker.build:
  extends: [ .os.docker.dind ]

  variables:
    dockerfile: "Dockerfile"
    context: "."
    image: ""
    build_version: "build-$CI_PIPELINE_IID"

  script:
    - >
      docker build
      --build-arg BUILDKIT_INLINE_CACHE=1
      --cache-from $CI_REGISTRY_IMAGE/$image:latest 
      --tag $CI_REGISTRY_IMAGE/$image:$build_version
      --pull
      -f "$dockerfile"
      "$context"
    - docker push $CI_REGISTRY_IMAGE/$image:$build_version

  interruptible: true

# Renames a Docker image in the GitLab Container Registry.
#
# Variables:
# - image: name of the created image
# - old_version: current tag of the image (default: IID of the pipeline with the prefix "build-")
# - new_version: new tag of the image (default: "latest")
#
# See also the variables from:
# - os.docker.dind
.os.docker.rename:
  extends: [ .os.docker.dind ]

  variables:
    image: ""
    old_version: "build-$CI_PIPELINE_IID"
    new_version: "latest"
    GIT_STRATEGY: none

  script:
    - docker pull $CI_REGISTRY_IMAGE/$image:$old_version
    - docker tag $CI_REGISTRY_IMAGE/$image:$old_version $CI_REGISTRY_IMAGE/$image:$new_version
    - docker push $CI_REGISTRY_IMAGE/$image:$new_version

  interruptible: true
  resource_group: docker-image-$image
