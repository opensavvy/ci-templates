# OpenSavvy CI

[![Stability: deprecated](https://badgen.net/static/Stability/deprecated/purple)](https://opensavvy.dev/open-source/stability.html)

This repository contains helpers that can be included in your own GitLab files.

We are not planning any new developments as part of this repository. We will be keeping it as-is to avoid breaking existing users, but do not expect new functionality.

We recommend to migrate to [.gitlab-ci.main.kts](https://opensavvy.gitlab.io/automation/gitlab-ci.kt/docs/).
